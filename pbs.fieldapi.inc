<?php

/**
 * @file
 * Per-bundle storage implementation using the Field Storage API "pre"
 * hooks and other Field API hooks.
 */

/**
 * @defgroup field_pbs Field API Per-Bundle Storage
 * @{
 * Store multiple Field API data fields in a wide table so that they
 * can be loaded for a single object in a single query instead of
 * requiring multiple queries or joins.
 */
/**
 * @} End of "defgroup field_pbs"
 */

/**
 * Generate a table name for a bundle data table.
 *
 * @param $entity_type
 *   The name of the entity_type.
 * @param $name
 *   The name of the bundle
 * @return
 *   A string containing the generated name for the database table
 */
function pbs_tablename($entity_type, $name) {
  // @todo Clarify this
  // There are some entity types which has just one bundle, then just return the name of the entity as name of the table.
  if ($entity_type == $name) {
    return 'field_pbs_' . $entity_type;
  }
  else  {
    return 'field_pbs_' . $entity_type . '_' . $name;
  }
}

/**
 * Generate a table name for a bundle revision archive table.
 *
 * @param $entity_type
 *   The name of the entity_type.
 * @param $name
 *   The name of the bundle
 * @return
 *   A string containing the generated name for the database table
 */
function pbs_revision_tablename($entity_type, $name) {
  if ($entity_type == $name) {
    return 'field_pbsr_' . $entity_type;
  }
  else  {
    return 'field_pbsr_' . $entity_type . '_' . $name;
  }
}

/**
 * Return the database schema for a bundle table and its corresponding
 * revision table.  A bundle table has columns to store every
 * non-unlimited field's data in the bundle.
 *
 * @param $entity_type
 *   The name of the entity_type.
 * @param $bundle
 *   The bundle name for which to create table schemas.
 * @return
 *   One or more tables representing the schema for the bundle tables.
 */
function pbs_bundle_schema($entity_type, $bundle) {
  $current = array(
    'description' => 'Bundle table for bundle '. $bundle,
    'fields' => array(
      'entity_type' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The entity type this data is attached to',
      ),
      'entity_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The entity id this data is attached to',
      ),
      'revision_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
      ),
      'language' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The language for this data',
      ),
    ),
    'primary key' => array('entity_type', 'entity_id', 'language'),
  );

  $instances = field_info_instances($entity_type, $bundle);
  foreach ($instances as $instance) {
    $field = field_info_field($instance['field_name']);
    $schema = (array) module_invoke($field['module'], 'field_schema', $field);
    if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
      for ($i = 0; $i < $field['cardinality']; ++$i) {
        foreach ($schema['columns'] as $name => $spec) {
          $column_name = $field['field_name'] .'_'. $name .'_'. $i;
          $current['fields'][$column_name] = $spec;
        }
      }
    }
  }

  // Construct the revision table.  The primary key includes
  // revision_id but not entity_id so that multiple revision loads can
  // use the IN operator.
  $revision = $current;
  $revision['description'] = 'Revision archive for bundle table for bundle '. $bundle;
  $revision['revision_id']['description'] = 'The entity revision id this data is attached to';
  $revision['primary key'] = array('entity_type', 'revision_id', 'language');

  return array(
    pbs_tablename($entity_type, $bundle) => $current,
    pbs_revision_tablename($entity_type, $bundle) => $revision,
  );
}

/**
 * Create the per-bundle storage table for a bundle if it does not
 * already exist.
 *
 * @param $entity_type
 *   The name of the entity_type.
 * @param $bundle
 *   The name of the bundle.
 */
function pbs_create_bundle_tables($entity_type, $bundle) {
  $schema = pbs_bundle_schema($entity_type, $bundle);
  foreach ($schema as $name => $table) {
    if (!db_table_exists($name)) {
      db_create_table($name, $table);
    }
  }
}

/**
 * Pre-compute the mapping of bundle table column name to field, item,
 * and delta.  This must be called any time the set of field instances
 * changes in any way.
 *
 * Suppose that a field name F, id Fid, with columns C1 and C2 and
 * with cardinality 2 has instances for bundles B1 and B2.  For field
 * F, the map will contain:
 *
 * $map = array(
 *   'entity_type' => array(
 *     'B1' => array(
 *       'F_C1_1' => array('F', 'Fid', 'C1', 1),
 *       'F_C1_2' => array('F', 'Fid', 'C1', 2),
 *       'F_C2_1' => array('F', 'Fid', 'C2', 1),
 *       'F_C2_2' => array('F', 'Fid', 'C2', 2),
 *       // other entries for B1
 *     ),
 *     'B2' => array(
 *       'F_C1_1' => array('F', 'Fid', 'C1', 1),
 *       'F_C1_2' => array('F', 'Fid', 'C1', 2),
 *       'F_C2_1' => array('F', 'Fid', 'C2', 1),
 *       'F_C2_2' => array('F', 'Fid', 'C2', 2),
 *       // other entries for B2
 *     ),
 *   ),
 * );
 */
function pbs_precompute_bundle_map() {
  $map = array();

  // Gather fields to load for each bundle.
  $entity_bundles = field_info_bundles();
  foreach ($entity_bundles as $entity_type => $bundles) {
    foreach ($bundles as $bundle => $label) {
      $instances = field_info_instances($entity_type, $bundle);
      foreach ($instances as $instance) {
        $field = field_info_field($instance['field_name']);
        if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
          $schema = (array) module_invoke($field['module'], 'field_schema', $field);
          for ($delta = 0; $delta < $field['cardinality']; ++$delta) {
            foreach ($schema['columns'] as $name => $spec) {
              $column_name = _field_sql_storage_columnname($field['field_name'], $name) .'_'. $delta;
              $map[$entity_type][$bundle][$column_name] = array($field['field_name'], $field['id'], $name, $delta);
            }
          }
        }
      }
    }
  }

  variable_set('pbs_field_map', $map);
  return $map;
}

/**
 * Returns the used pbs column name for the given field name, column, and delta.
 *
 * @param $field_name
 *  The name of the field.
 * @param $column
 *  The name of the field column.
 * @param $delta
 *  Field delta.
 */
function pbs_columname($field_name, $field_column, $delta) {
  $field_map = variable_get('pbs_field_map', array());

  foreach ($field_map as $entity_type => $bundles) {
    foreach ($bundles as $bundle => $columns) {
      foreach ($columns as $column => $data) {
        if ($data[0] == $field_name && $data[2] == $field_column && $data[3] == $delta) {
          return $column;
        }
      }
    }
  }
  return FALSE;
}

/**
 * Initialize a single bundle table from its constituent field
 * tables.
 *
 * @param $entity_type
 *   The name of the entity_type.
 * @param $bundle
 *   The bundle to initialize.
 */
function pbs_initialize_bundle($entity_type, $bundle) {
  $bundle_table = pbs_tablename($entity_type, $bundle);
  $revision_table = pbs_revision_tablename($entity_type, $bundle);

  $instances = field_info_instances($entity_type, $bundle);
  foreach ($instances as $instance) {
    $field = field_info_field($instance['field_name']);
    if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
      $field_table = _field_sql_storage_tablename($field);
      $field_revision_table = _field_sql_storage_revision_tablename($field);
      _pbs_initialize_bundle_field($bundle, $field, 'entity_id', $bundle_table, $field_table);
      _pbs_initialize_bundle_field($bundle, $field, 'entity_id', $revision_table, $field_revision_table);
    }
  }
}

function _pbs_initialize_bundle_field($bundle, $field, $id_key, $bundle_table, $field_table) {
  $pkey = array('entity_type' => 0, $id_key => 1, 'language' => 2);
  $results = db_select($field_table, 't', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('t')
    ->condition('bundle', $bundle)
    ->condition('deleted', 0)
    ->execute();
  foreach ($results as $row) {
    $delta = $row['delta'];
    unset($row['bundle'], $row['delta'], $row['deleted']);
    $keys = array_intersect_key($row, $pkey);
    foreach ($field['columns'] as $name => $spec) {
      $column_name = $field['field_name'] .'_'. $name;
      $row[$column_name .'_'. $delta] = $row[$column_name];
      unset($row[$column_name]);
    }
    db_merge($bundle_table)->fields($row)->key($keys)->execute();
  }
}

/**
 * Implementation of hook_field_attach_pre_load.
 *
 * Load available fields from an object's bundle table, preventing
 * them from being loaded from a field table.
 */
function pbs_field_attach_pre_load($entity_type, $entities, $age, &$skip_fields, $options) {
  $load_current = $age == FIELD_LOAD_CURRENT;

  // Gather ids needed for each bundle.
  $bundle_ids = array();
  $delta_count = array();
  foreach ($entities as $entity) {
    list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
    $bundle_ids[$bundle][] = $load_current ? $id : $vid;
  }

  // Get the fields to load for each bundle.
  $field_map = variable_get('pbs_field_map', array($entity_type => array()));
  $bundle_fields = $field_map[$entity_type];

  $new_skips = array();

  foreach ($bundle_ids as $bundle => $ids) {
    // If objects come in with a $bundle that does not exist any more,
    // there will not be any data for them, and $bundle_fields[$bundle]
    // will not exist.
    if (!isset($bundle_fields[$bundle])) {
      continue;
    }

    // Load the data for all objects in $bundle.
    $table = $load_current ? pbs_tablename($entity_type, $bundle) : pbs_revision_tablename($entity_type, $bundle);
    $results = db_select($table, 't')
      ->fields('t')
      ->condition('entity_type', $entity_type)
      ->condition($load_current ? 'entity_id' : 'revision_id', $ids, 'IN')
      ->execute();

    // For each row, for every field in the bundle, construct field
    // data items from the columns for that field, and save the items
    // keyed by object id and field name.
    foreach ($results as $row) {
      foreach ($bundle_fields[$bundle] as $column_name => $tuple) {
        list($field_name, $field_id, $item_name, $delta) = $tuple;

        if (!isset($skip_fields[$field_id]) && (!isset($options['field_id']) || $options['field_id'] == $field_id)) {
          if (!is_null($row->{$column_name})) {
            // We must explicitly include $delta in the assignment
            // below.  Consider a field 'foo' with two columns, e.g. value
            // and format. Without using $delta, the row containing
            // foo_value_0 and foo_format_0 would end up stored as
            // $obj->foo[0]['value'] and $obj->foo[1/*BZZZ!*/]['format'].
            $entities[$row->entity_id]->{$field_name}[$row->language][$delta][$item_name] = $row->{$column_name};
          }
          else if (!isset($entities[$row->entity_id]->{$field_name})) {
            // Even if we have no data for a field, $obj->field should
            // always at least be an empty array.  Do NOT put a
            // language key in; if we have no data for a field, it is wrong to
            // create an empty language key for it.
            $entities[$row->entity_id]->{$field_name} = array();
          }
          $new_skips[$field_id] = 1;
        }
      }
    }
  }

  $skip_fields += $new_skips;
}

/**
 * Implementation of field_attach_pre_update.
 *
 * When an object's fields are saved, update its bundle table.
 */
function pbs_field_attach_pre_update($entity_type, $entity, &$skip_fields) {
  list($id, $vid, $bundle) = field_extract_ids($entity_type, $entity);
  $bundle_table = pbs_tablename($entity_type, $bundle);
  $revision_table = pbs_revision_tablename($entity_type, $bundle);
  $key = array('entity_type' => $entity_type, 'entity_id' => $id, 'revision_id' => $vid);
  $all_fields = array();
  $rows = array();

  $instances = field_info_instances($bundle);
  foreach ($instances as $instance) {
    // The Rules:
    // * $object->field_name unset: leave data untouched
    // * $object->field_name = array()/NULL: empty the field for all
    //   (enabled) languages
    // * $object->field_name[$lang] unset: leave data untouched for $lang
    // * $object->field_name[$lang] = array()/NULL: empty the field for $lang
    //
    // Enabled/available languages for a single field are returned by
    // field_multilingual_available_languages.

    // Function property_exists() is slower, so we catch the more frequent cases
    // where it's an empty array with the faster isset().
    $field_name = $instance['field_name'];
    $field = field_info_field($instance['field_name']);
    $field_id = $field['id'];
    if (!isset($skip_fields[$field['id']]) && $field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
      if (isset($entity->$field_name) || property_exists($entity, $field_name)) {
        // For each field, we are given data for one or more language
        // keys. The rule is to update data for the languages we're
        // given, and leave alone data for languages we're not
        // given. We write all the data for each language on a single
        // row, but we do not want to assume we have the same language
        // keys for every field and cannot assume that we have all the
        // fields for any language for which we have one field.
        //
        // So, we build up an array $rows keyed by language. Each
        // $rows[$language] contains all the columns for all the
        // fields which have data for that language, and no entries
        // for the other columns.

        // TODO: The field might be NULL or array().  I need to figure out the
        // correct behavior in that case; I think it will be UPDATEing
        // all language rows for the object to have NULL for that
        // field.  For now, just avoid an exception.
        if (empty($entity->{$field_name})) {
          // if $object->$field_name is null/empty I think all you
          // need to do is set NULL the field values for every row
          // whose language is in the set returned by
          // field_multilingual_available_languages($obj_type, $field)
          $langs_items = array_fill_keys(field_multilingual_available_languages($entity_type, $field), array());
        }
        else {
          $langs_items = $entity->{$field_name};
        }

        foreach ($langs_items as $language => $items) {
          // $obj->field[lang] can be NULL
          if ($items === NULL) {
            $items = array();
          }

          // field_attach_load() is specified to return data items indexed
          // from delta 0, regardless of how they were provided on
          // save.  It is more efficient to re-index on save than on load.
          $items = array_merge($items);
          for ($delta = 0; $delta < $field['cardinality']; ++$delta) {
            foreach ($field['columns'] as $name => $spec) {
              $rows[$language][_field_sql_storage_columnname($field['field_name'], $name) .'_'. $delta] = isset($items[$delta][$name]) ? $items[$delta][$name] : NULL;
            }
          }
        }
      }
    }
  }

  foreach ($rows as $language => $row) {
    if (count($row)) {
      $row['language'] = $key['language'] = $language;
      db_merge($bundle_table)->fields($row)->key($key)->execute();
      if (isset($vid)) {
        db_merge($revision_table)->fields($row)->key($key)->execute();
      }
    }
  }
}

/**
 * Implementation of hook_field_attach_pre_insert.
 *
 * When an object's fields are saved, update its bundle table.
 */
function pbs_field_attach_pre_insert($entity_type, $entity, &$skip_fields) {
  return pbs_field_attach_pre_update($entity_type, $entity, $skip_fields);
}

/**
 * Implementation of hook_field_attach_delete.
 */
function pbs_field_attach_delete($entity_type, $entity) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  $bundle_table = pbs_tablename($entity_type, $bundle);
  $revision_table = pbs_revision_tablename($entity_type, $bundle);
  db_delete($bundle_table)->condition('entity_type', $entity_type)->condition('entity_id', $id)->execute();
  db_delete($revision_table)->condition('entity_type', $entity_type)->condition('entity_id', $id)->execute();
}

/**
 * Implementation of field_attach_delete_revision.
 */
function pbs_field_attach_delete_revision($entity_type, $entity) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  $revision_table = pbs_revision_tablename($entity_type, $bundle);
  db_delete($revision_table)->condition('entity_type', $entity_type)->condition('entity_id', $id)->condition('revision_id', $vid)->execute();
}

/**
 * Implementation of hook_field_create_instance.
 *
 * When a field is added to a bundle, add the new columns to the
 * bundle table.
 */
function pbs_field_create_instance($instance) {
  pbs_precompute_bundle_map();
  $entity_type = $instance['entity_type'];

  // If the bundle table does not exist yet, create it now.  The
  // initial schema for the bundle table will include the columns for
  // the instance just created; we do not need to do it again here.
  if (!db_table_exists(pbs_tablename($entity_type, $instance['bundle']))) {
    pbs_create_bundle_tables($entity_type, $instance['bundle']);
    return;
  }

  $field = field_info_field($instance['field_name']);
  if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
    $bundle_table = pbs_tablename($entity_type, $instance['bundle']);
    $revision_table = pbs_revision_tablename($entity_type, $instance['bundle']);
    $schema = (array) module_invoke($field['module'], 'field_schema', $field);
    for ($i = 0; $i < $field['cardinality']; ++$i) {
      foreach ($schema['columns'] as $name => $spec) {
        db_add_field($bundle_table, $field['field_name'] .'_'. $name .'_'. $i, $spec);
        db_add_field($revision_table, $field['field_name'] .'_'. $name .'_'. $i, $spec);
      }
    }
  }
}

/**
 * Implementation of hook_field_delete_instance.
 *
 * When a field is removed from a bundle, drop the columns and update
 * the bundle map. We have to remove the columns immediately because a
 * new field with the same name may be created later.
 *
 * To support being primary storage, we would have rename the columns
 * instead and clean them up on batch delete.
 */
function pbs_field_delete_instance($instance) {
  $ret = array();

  $field = field_info_field($instance['field_name']);
  if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
    $bundle_table = pbs_tablename($instance['entity_type'], $instance['bundle']);
    $revision_table = pbs_revision_tablename($instance['bundle']);
    $schema = (array) module_invoke($field['module'], 'field_schema', $field);
    for ($i = 0; $i < $field['cardinality']; ++$i) {
      foreach ($schema['columns'] as $name => $spec) {
        db_drop_field($ret, $bundle_table, $instance['field_name'] .'_'. $name .'_'. $i);
        db_drop_field($ret, $revision_table, $instance['field_name'] .'_'. $name .'_'. $i);
      }
    }
  }

  pbs_precompute_bundle_map();
}

/**
 * Implementation of hook_field_attach_create_bundle.
 *
 * Create the (empty) bundle tables.
 */
function pbs_field_attach_create_bundle($entity_type, $bundle) {
  pbs_precompute_bundle_map();
  pbs_create_bundle_tables($entity_type, $bundle);
}

/**
 * Implementation of hook_field_attach_rename_bundle.
 *
 * Rename the bundle tables.
 */
function pbs_field_attach_rename_bundle($entity_type, $old_bundle, $new_bundle) {
  $ret = array();
  db_rename_table($ret, pbs_tablename($entity_type, $old_bundle), pbs_tablename($entity_type, $new_bundle));
  db_rename_table($ret, pbs_revision_tablename($entity_type, $old_bundle), pbs_revision_tablename($entity_type, $new_bundle));
  pbs_precompute_bundle_map();
}

/**
 * Implementation of hook_field_attach_delete_bundle.
 *
 * When a bundle is deleted, drop the bundle tables and update
 * the bundle map.  We have to drop the tables immediately because a
 * new bundle with the same name may be created later.
 *
 * To support being primary storage, we would have rename the tables
 * instead and clean them up on batch delete.
 */
function pbs_field_attach_delete_bundle($entity_type, $bundle, $instances) {
  $ret = array();
  db_drop_table($ret, pbs_tablename($entity_type, $bundle));
  db_drop_table($ret, pbs_revision_tablename($entity_type, $bundle));
  pbs_precompute_bundle_map();
}

/**
 * Implements hook_field_storage_query().
 */
function pbs_field_storage_query(EntityFieldQuery $query) {
  $table_aliases = array();
  $entity_type = $query->entityConditions['entity_type']['value'];
  $bundle = $query->entityConditions['bundle']['value'];
  // The base table contains only fields on the specified entity type & bundle
  // so adding those conditions to the WHERE clause is redundant.
  unset($query->entityConditions['entity_type']);
  unset($query->entityConditions['bundle']);

  if ($query->age == FIELD_LOAD_CURRENT) {
    $base_table = pbs_tablename($entity_type, $bundle);
    $tablename_function = '_field_sql_storage_tablename';
    $id_key = 'entity_id';
  }
  else {
    $base_table = pbs_revision_tablename($entity_type, $bundle);
    $tablename_function = '_field_sql_storage_revision_tablename';
    $id_key = 'revision_id';
  }

  $select_query = db_select($base_table, 'pbs');
  $select_query->addTag('entity_field_access');
  $select_query->addMetaData('base_table', $base_table);
  $select_query->addExpression(':entity_type', 'entity_type', array(':entity_type' => $entity_type));
  $select_query->addExpression(':bundle', 'bundle', array(':bundle' => $bundle));
  $select_query->fields('pbs', array('entity_id', 'revision_id'));

  // Is there a need to order by a multivalue field? Since each delta is stored
  // in a separate column, that type of ordering is impossible in pbs storage,
  // so the original field table needs to be joined in.
  $join = array();
  foreach ($query->order as $order) {
    $specifier = $order['specifier'];
    if ($order['type'] == 'field' && $specifier['field']['cardinality'] > 1) {
      $join[] = $specifier['index'];
    }
  }
  // Fields with an unlimited number of values are stored in their own table,
  // which means that they also need to be joined in.
  foreach ($query->fields as $index => $field) {
    if ($field['cardinality'] == FIELD_CARDINALITY_UNLIMITED || in_array($index, $join)) {
      $tablename = $tablename_function($field);
      $table_alias = $tablename . $index;
      $table_aliases[$index] = $table_alias;

      $select_query->join($tablename, $table_alias, "$table_alias.entity_type = pbs.entity_type AND $table_alias.$id_key = pbs.$id_key");
      $select_query->distinct();
    }
  }

  foreach ($query->entityConditions as $key => $condition) {
    $query->addCondition($select_query, "pbs.$key", $condition);
  }
  foreach ($query->fieldConditions as $index => $condition) {
    $field = $condition['field'];
    // The field is in its own table.
    if (isset($table_aliases[$index])) {
      $table_alias = $table_aliases[$index];
      $sql_field = "$table_alias." . _field_sql_storage_columnname($field['field_name'], $condition['column']);
      $query->addCondition($select_query, $sql_field, $condition);
      _pbs_add_group_conditions($select_query, $table_alias, $condition);
    }
    else {
      // If the field has multiple values (which means multiple db columns),
      // each column needs to be compared (and conditions connected with OR).
      $conditions = db_or();
      for ($delta = 0; $delta < $field['cardinality']; ++$delta) {
        $sql_field = "pbs." . pbs_columname($field['field_name'], $condition['column'], $delta);
        _pbs_add_condition($conditions, $sql_field, $condition);
      }
      $select_query->condition($conditions);
    }
  }

  // Is there a need to sort the query by a property? That requires the entity
  // table to be joined in.
  $has_property_order = FALSE;
  foreach ($query->order as $order) {
    if ($order['type'] == 'property') {
      $has_property_order = TRUE;
    }
  }
  if ($query->propertyConditions || $has_property_order) {
    $entity_base_table = _field_sql_storage_query_join_entity($select_query, $entity_type, 'pbs');
    foreach ($query->propertyConditions as $property_condition) {
      $query->addCondition($select_query, "$entity_base_table." . $property_condition['column'], $property_condition);
    }
  }

  // Order the query.
  foreach ($query->order as $order) {
    if ($order['type'] == 'entity') {
      $key = $order['specifier'];
      $select_query->orderBy("pbs.$key", $order['direction']);
    }
    elseif ($order['type'] == 'field') {
      $specifier = $order['specifier'];
      $field = $specifier['field'];
      $index = $specifier['index'];
      // The original field tale is being used (multivalue or unlimited field).
      if (isset($table_aliases[$index])) {
        $table_alias = $table_aliases[$index];
        $sql_field = "$table_alias." . _field_sql_storage_columnname($field['field_name'], $specifier['column']);
        $select_query->orderBy($sql_field, $order['direction']);
      }
      else {
        $sql_field = "pbs." . pbs_columname($field['field_name'], $specifier['column'], 0);
        $select_query->orderBy($sql_field, $order['direction']);
      }
    }
    elseif ($order['type'] == 'property') {
      $select_query->orderBy("$entity_base_table." . $order['specifier'], $order['direction']);
    }
  }

  return $query->finishQuery($select_query, $id_key);
}

/**
 * Adds delta / language group conditions to the query.
 *
 * @param SelectQuery $select_query
 *   A SelectQuery object.
 * @param $table_alias
 *   Current table alias.
 * @param $condition
 *   A condition as described in EntityFieldQuery::fieldCondition().
 */
function _pbs_add_group_conditions(SelectQuery $select_query, $table_alias, $condition) {
  static $groups = array();

  foreach (array('delta', 'language') as $column) {
    if (isset($condition[$column . '_group'])) {
      $group_name = $condition[$column . '_group'];
      if (!isset($groups[$column][$group_name])) {
        $groups[$column][$group_name] = $table_alias;
      }
      else {
        $select_query->where("$table_alias.$column = " . $groups[$column][$group_name] . ".$column");
      }
    }
  }
}

/**
  * Adds a condition to a DatabaseCondition object (used for "OR"-ing).
  *
  * This is a helper for pbs_field_storage_query(), an EntityFieldQuery
  * implementation. Based on the addCondition method from entity.inc.
  *
  * @param $conditions
  *   A DatabaseCondition object returned by db_or().
  * @param $sql_field
  *   The name of the field.
  * @param $condition
  *   A condition as described in EntityFieldQuery::fieldCondition().
  */
function _pbs_add_condition(DatabaseCondition $conditions, $sql_field, $condition) {
  $like_prefix = '';
  switch ($condition['operator']) {
    case 'CONTAINS':
      $like_prefix = '%';
    case 'STARTS_WITH':
      $conditions->condition($sql_field, $like_prefix . db_like($condition['value']) . '%', 'LIKE');
      break;
    default:
      $conditions->condition($sql_field, $condition['value'], $condition['operator']);
  }
}
